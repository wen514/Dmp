package com.dmp.config

import java.util
import java.util.Properties

import com.typesafe.config.{Config, ConfigFactory, ConfigObject}

object ConfigHandler {
  private lazy val config: Config = ConfigFactory.load()

  val parquetFilePath = config.getString("parquet.file.path")

  val appdictFilePath: String = config.getString("app.file.path")
  val driverClass: String = config.getString("jdbc.mysql.class")
  val url: String = config.getString("jdbc.mysql.url")
  val user: String = config.getString("jdbc.mysql.user")
  val pass: String = config.getString("jdbc.mysql.pass")

  val redisHost: String = config.getString("redis.host")
  val redisPort: Int = config.getInt("redis.port")
  val prop = new Properties()
  prop.setProperty("driver", driverClass)
  prop.setProperty("user", user)
  prop.setProperty("password", pass)
  val dCode2Name: util.Map[String, AnyRef] = config.getObject("dcode2name").unwrapped()
  val osCode2Name: util.Map[String, AnyRef] = config.getObject("oscode2name").unwrapped()

  /*
  * 生成标签的编号
  * */
  //操作系统
  val osCode2Code = config.getObject("oscode2code").unwrapped()
  //联网方式
  val netName2Code = config.getObject("netname2code").unwrapped()
  //运营商
  val orName2Code = config.getObject("orname2code").unwrapped()

}
