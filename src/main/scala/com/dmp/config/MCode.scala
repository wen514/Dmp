package com.dmp.config

import java.io.FileInputStream
import java.util
import java.util.Properties

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Properties

object MCode {
  val prop = new Properties()
  val path = Thread.currentThread().getContextClassLoader.getResource("code.properties").getPath
  prop.load(new FileInputStream(path))
}
