package com.dmp.report

import com.dmp.config.{ConfigHandler, MCode}
import com.dmp.util.RptKpi
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}

object ReportMonitor {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    val sparkConf = new SparkConf()
    sparkConf.setAppName("地域分布情况 MR版本")
    sparkConf.setMaster("local[*]")
    sparkConf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    val sc = new SparkContext(sparkConf)
    val sQLContext = new SQLContext(sc)
    val dataFrame: DataFrame = sQLContext.read.parquet(ConfigHandler.parquetFilePath)
    /*
    * 终端设备
    * */
   RptKpi.makeCommonDataFrame(dataFrame,sQLContext,"ispname","orc_report_va")
    /*
    * 网络类型
    * */
    RptKpi.makeCommonDataFrame(dataFrame,sQLContext,"networkmannername","orc_report_network")
    /*
    * 设备类型
    * */
    RptKpi.areaRportParameterInt(dataFrame,sQLContext,"orc_report_equipment","devicetype",ConfigHandler.dCode2Name)
    /*
    *操作系统
    * */
    RptKpi.areaRportParameterInt(dataFrame,sQLContext,"orc_report_OS","client",ConfigHandler.osCode2Name)

    /*
    *媒体分析
    * */
    RptKpi.AppAnalysisCore(sc,dataFrame,"appname","appid","F:/Violet/report/app")
    //appid
    ///dataFrame.registerTempTable("dmp")
    //sQLContext.sql("select  devicetype from dmp").show(1000)
  }
}
