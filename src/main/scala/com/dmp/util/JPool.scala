package com.dmp.util

import com.dmp.config.ConfigHandler
import redis.clients.jedis.JedisPool

object JPool {
    private val jedisPool = new JedisPool(ConfigHandler.redisHost,ConfigHandler.redisPort)

  //获取连接池
  def getJedis ={
    jedisPool.getResource
  }
}
