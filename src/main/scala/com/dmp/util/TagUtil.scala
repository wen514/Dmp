package com.dmp.util

import org.apache.commons.lang.StringUtils
import org.apache.spark.sql.Row

object TagUtil {
    val userIfFilterCondition=
      """
        |imei!="" or imei!=null or
        |mac!="" or mac!=null or
        |idfa!="" or idfa != null or
        |androidid!="" or androidid != null or
        |openudid!="" or openudid != null or
        |imeimd5!="" or imeimd5 != null or
        |macmd5!="" or macmd5 != null or
        |idfamd5!="" or idfamd5 != null or
        |androididmd5!="" or androididmd5 != null or
        |openudidmd5!="" or openudidmd5 != null or
        |imeisha1!="" or imeisha1 != null or
        |macsha1!="" or macsha1 != null or
        |idfasha1!="" or idfasha1 != null or
        |androididsha1!="" or androididsha1 != null or
        |openudidsha1!="" or openudidsha1 != null
      """.stripMargin
  /*
  * 模式匹配
  * */
  def batainUserOneId(row:Row)={
    row match{
      case v if StringUtils.isNotEmpty(v.getAs[String]("imei")) => v.getAs[String]("imei")
      case v if StringUtils.isNotEmpty(v.getAs[String]("mac")) => v.getAs[String]("mac")
      case v if StringUtils.isNotEmpty(v.getAs[String]("idfa")) => v.getAs[String]("idfa")
      case v if StringUtils.isNotEmpty(v.getAs[String]("androidid")) => v.getAs[String]("androidid")
      case v if StringUtils.isNotEmpty(v.getAs[String]("openudid")) => v.getAs[String]("openudid")
      case v if StringUtils.isNotEmpty(v.getAs[String]("imeimd5")) => v.getAs[String]("imeimd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("macmd5")) => v.getAs[String]("macmd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("idfamd5")) => v.getAs[String]("idfamd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("androididmd5")) => v.getAs[String]("androididmd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("openudidmd5")) => v.getAs[String]("openudidmd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("imeisha1")) => v.getAs[String]("imeisha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("macsha1")) => v.getAs[String]("macsha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("idfasha1")) => v.getAs[String]("idfasha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("androididsha1")) => v.getAs[String]("androididsha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("openudidsha1")) => v.getAs[String]("openudidsha1")
    }
  }
}
