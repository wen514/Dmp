package com.oracle.beans

 case class RptArea(
                     val provinceName: String,
                     val cityName: String,
                     val adRawReq: Int,
                     val adEffReq: Int,
                     val adReq: Int,
                     val adRbtReq: Int,
                     val adSuccReq: Int,
                     val adShowReq: Int,
                     val adClickReq: Int,
                     val adCost: Double,
                     val adConsumption: Double

                   )
case class Rpt(
                                    val va:String,
                                    val adRawReq: Int,
                                    val adEffReq: Int,
                                    val adReq: Int,
                                    val adRbtReq: Int,
                                    val adSuccReq: Int,
                                    val adShowReq: Int,
                                    val adClickReq: Int,
                                    val adCost: Double,
                                    val adConsumption: Double
              )

case class RptCode(
                val code:String,
                val adRawReq: Int,
                val adEffReq: Int,
                val adReq: Int,
                val adRbtReq: Int,
                val adSuccReq: Int,
                val adShowReq: Int,
                val adClickReq: Int,
                val adCost: Double,
                val adConsumption: Double
              )