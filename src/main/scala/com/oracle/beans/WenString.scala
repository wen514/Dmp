package com.oracle.beans

class WenString(val str: String) {

  def toIntPlus = {
    try {
      this.str.toInt
    } catch {
      case _: Exception => 0
    }
  }

  def toDoublePlus = {
    try {
      this.str.toDouble
    } catch {
      case _: Exception => 0d
    }
  }
}

object WenString {

  implicit def StrCrossOverSheep(str: String) = new WenString(str)

}
