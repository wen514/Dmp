package com.oracle.tools

import com.dmp.config.ConfigHandler
import com.dmp.util.JPool
import org.apache.commons.lang.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import redis.clients.jedis.Jedis

object Appdice2Redis {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    val conf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("将app名称映射存入redis")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    val sc = new SparkContext(conf)

    //读取字典数据
    sc.textFile(ConfigHandler.appdictFilePath)
      .map(_.split("\t",-1))
      .filter(_.length>=5)
      .foreachPartition(iter=>{
        val jedis: Jedis = JPool.getJedis
        iter.foreach(tp=>{
          val appName=tp(1)
          val appId=tp(4)
          if(StringUtils.isNotEmpty(appId)) jedis.hset("appdict",appId,appName)
        })
        jedis.close()
      })
    sc.stop()
  }
}
